FROM ubuntu:latest

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt install -y \
  texlive \
  texlive-latex-extra \
  texlive-xetex \
  python3 \
  python3-pip \
  fonts-roboto

RUN apt install -y pandoc

RUN pip3 install --upgrade pip && pip3 install --no-cache-dir nbconvert
